<?php declare(strict_types = 1);

namespace App\Components;

use NeoVance\Component;

class Head extends Component {
    public function render() { ?>
        <head>
            <title><?php echo $this->props->title ?></title>
        </head>
<?php }
}
