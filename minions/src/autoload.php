<?php declare(strict_types = 1);
require_once '/composer/bootstrap.php';
require_once '/application/minion/vendor/autoload.php';

$loader = new \Aura\Autoload\Loader;
$loader->register();
$loader->setPrefixes([
    'Model' => __DIR__ . '/Model',
    'Provider' => __DIR__ . '/Provider',
    'App' => __DIR__ . '/../app',
    'NeoVance' => __DIR__ . '/../neovance'
]);
