<?php declare(strict_types = 1);

$vendors = realpath(__DIR__ . '/vendor');
require_once("{$vendors}/autoload.php");

error_reporting(E_ALL);
ini_set('short_open_tag', '1');

$environment = 'development';

/**
* Register the error handler
*/
$whoops = new Whoops\Run;
if ($environment !== 'production') {
    if(php_sapi_name() !== 'cli') {
        $whoops->pushHandler(new Whoops\Handler\PrettyPageHandler);
    } else {
        $whoops->pushHandler(function($e) {
            print_r($e->getMessage()."\n");
        });
    }
} else {
    $whoops->pushHandler(function($e){
        echo 'Todo: Friendly error page and send an email to the developer';
    });
}
$whoops->register();
