# Phunyuns

Right now we are in a bit of a weird state since I have had to customize the minions library in order to handle some of the things I wanted to do, and I had not set up any repos. To get everything running you will need to use composer to install dependencies in two locations.

---

## Dependencies

* First the main framework dependencies. `cd composer && composer install`
* Then the minion library dependencies. `cd ../minions/minion && composer install`

---

## Starting

Once dependencies are installed locally you can stand up the docker images using `docker-compose up` from the root of the project.

---

## HTTP

If you want to create a service provider that has an HTTP API extend `Provider\HttpProvider` or `Provider\HttpResourceProvider`. The Application instance is passed into the `httpAttach` function which can be used to map routes for the http service.

Naming your docker container creates the external route automatically. Requests to `/api/v{version}/{container_name}/{...etc}` are automatically proxied to the container, where the new request path is simply `/v{version}/{...etc}`. The Application is automatically created using the base path of `/v{version}` so all route paths can be defined using only the path components you care about. See an example in `Provider/Authorization`.

TODO: Create other base HTTP Provider that will make defining routes easier.

---

## WAMP

TODO
