<?php declare(strict_types = 1);

namespace Provider;

use \NeoVance\Application;

abstract class HttpResourceProvider extends HttpProvider {

    protected $hostname = 'localhost';
    protected $resource;

    public function httpAttach(Application $app) {
        $app->attach("{$this->resource}.", '', [$this, 'httpMap']);
    }

    public function httpMap($map) {
        // TODO: Set legit .env imported hostname
        // $map->host("{account}.?{$this->hostname}");

        $map->tokens([
            'id' => '\d+',
            'offset' => '\d+',
            'limit' => '\d{1,4}',
            'account' => '[a-z0-9-]+'
        ]);

        $map->defaults([
            'id' => 0,
            'offset' => 0,
            'limit' => 100,
            'account' => null
        ]);
        
        $map->post('create', '/', [$this, 'httpCreate']);
        $map->get('single', '/{id}', [$this, 'httpGet']);
        $map->get('query', '/query/{query}', [$this, 'httpQuery']);
        $map->get('list', '/list{/offset,limit}', [$this, 'httpList']);
        $map->put('replace', '/{id}', [$this, 'httpReplace']);
        $map->patch('edit', '/{id}', [$this, 'httpEdit']);
        $map->delete('delete', '/{id}', [$this, 'httpDelete']);
    }

    public function getResourceModel() {
        $resourceType = ucfirst($this->resource);
        $resourceType = "\Model\{$resourceType}";
        return new $resourceType();
    }

    public function createResource($props) {
        $resource = $this->getResourceModel();

        return $this->updateResource($props, $resource);
    }

    public function updateResource($props, $resource) {
        foreach($props as $key => $value) {
            $resource->$key = $value;
        }

        $resourse->save();
        
        return $resource;
    }

    abstract public function httpCreate($request, $response);
    abstract public function httpGet($request, $response);
    abstract public function httpQuery($request, $response);
    abstract public function httpList($request, $response);
    abstract public function httpReplace($request, $response);
    abstract public function httpEdit($request, $response);
    abstract public function httpDelete($request, $response);        

}
