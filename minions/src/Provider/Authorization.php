<?php declare(strict_types = 1);

namespace Provider;

use \React\Promise\Deferred;
use \NeoVance\Application;
use \Model\User;
use \Model\Account;
use \Model\Session;
use \Model\Resource;
use \Common\PermissionLevel;
use \Common\PermissionType;
use \Dflydev\FigCookies\SetCookie;
use \Dflydev\FigCookies\FigResponseCookies;

class Authorization extends HttpProvider {

    protected $prefix = 'nv.internal.';

    public function boot() {
        $this->getClient()->getSession()
            ->subscribe('wamp.session.on_join', [$this, 'onJoin']);
        $this->getClient()->getSession()
            ->subscribe('wamp.session.on_leave', [$this, 'onLeave']);

        $this->register('authorize', 'authorize');
    }

    public function onJoin($args, $kwArgs, $details) {
        $data = array_shift($args);
        $session = new Session;
        $user = $this->queryUser($data->authextra->host, $data->authid);

        if (!$user) {
            return;
        }

        $session->resource = $data->session;
        $session->user = $user;
        $session->account = $user->account;
        $session->save();
    }

    public function onLeave($args, $kwArgs, $details) {
        $data = array_shift($args);
        $session = new Session;

        $session->load(['resource = ?', $data], null, 0);

        if (!$session->dry()) {
            $session->erase();
        }
    }

    public function authorize($args) {        
        $session  = array_shift($args);
        $uri = array_shift($args);
        $action = array_shift($args);
        $options = array_shift($args);

        $userSession = new Session;
        $userSession->load(['resource = ?', $session->session]);

        if (!$userSession->dry()) {
            // return call_user_func_array(
            //     [$this, "{$action}Topic"],
            //     [$userSession, $uri, $options]
            // );
            $this->createResource($userSession, $uri);
            return [
                'allow' => $this->validateUri($userSession->account->name, explode('.', $uri)),
                'disclose' => true,
                'cache' => true
            ];
        }

        return false;
    }

    public function validateUri($account, $parts) {
        if (count($parts) > 2) {
            $ns = $parts[0];
            if ($ns !== 'nv') {
                return false;
            }

            $acns = $parts[1];
            if ($account !== $acns) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    public function createResource(Session $session, $uri, $permissions = 755) {
        $resources = new Resource;

        $found = $resources->findOne(['topic = ?', $uri]);

        $parts = explode('.', $uri);
        if (!$found && count($parts) > 2) {
            $end = array_pop($parts);
            $parent = $this->createResource($session, implode('.', $parts));

            $found = new Resource;
            $found->topic = $uri;
            $found->account = $session->account;
            $found->owner = $session->user;
            $found->group = $session->user->groups[0];
            $found->grant = $permissions;
            $found->save();
        }

        return $found;
    }

    public function hasPermission(Session $session, Resource $resource, $permission) {
        $owner = $permission * PermissionLevel::Owner;
        $group = $permission * PermissionLevel::Group;
        $world = $permission * PermissionLevel::World;
        $valid = false;

        if ($resource->owner->_id === $session->user->_id) {
            print_r($resource->grant & $owner);
            $valid = $resource->grant & $owner;
        }

        if(!$valid && $session->user->groups->contains($resource->group)) {
            print_r($resource->grant & $group);
            $valid = $resource->grant & $group;
        }
        
        if (!$valid) {
            print_r($resource->grant & $world);
            $valid = $resource->grant & $world;
        }

        return $valid;
    } 

    public function publishTopic(Session $session, $uri, $options) {
        $parts = explode('.', $uri);
        $valid = $this->validateUri($session->account->name, $parts);
        $end = array_pop($parts);

        if ($valid) {
            $actual = $this->createResource($session, $uri, 644);
            $valid = $this->hasPermission($session, $actual, PermissionType::Write);
        }

        return [
            'allow' => $valid,
            'disclose' => true,
            'cache' => true
        ];
    }

    public function subscribeTopic(Session $session, $uri, $options) {
        $valid = false;

        if ($options->match !== 'exact') {
            $uriLike = $uri;
            if ($options->match === 'wildcard') {
                $uriLike = preg_replace('/\.\./', '.%.', $uri);
            } else if ($options->match === 'prefix') {
                $uriLike = $uri . '%';
            }
            
            $resources = (new Resource)->find(['topic LIKE ?', $uriLike]);
            if ($resources) {
                foreach ($resources as $resource) {
                    $parts = explode('.', $resource->topic);
                    $valid = $this->validateUri($session->account->name, $parts);

                    if ($valid) {
                        $valid = $this->hasPermission($session, $resource, PermissionType::Read);
                    }

                    if (!$valid) {
                        break;
                    }
                }
            }
        } else {
            $parts = explode('.', $uri);
            $valid = $this->validateUri($session->account->name, $parts);

            if ($valid) {
                $actual = $this->createResource($session, $uri, 644);
                $valid = $this->hasPermission($session, $actual, PermissionType::Read);
            }
        }

        return [
            'allow' => $valid,
            'disclose' => true,
            'cache' => true
        ];
    }

    public function registerTopic(Session $session, $uri, $options) {
        return $this->publishTopic($session, $uri, $options);
    }

    public function callTopic(Session $session, $uri, $options) {
        $parts = explode('.', $uri);
        $valid = $this->validateUri($session->account->name, $parts);

        if ($valid) {
            $actual = $this->createResource($session, $uri, 755);
            $valid = $this->hasPermission($session, $actual, PermissionType::Execute);
        }

        return [
            'allow' => $valid,
            'disclose' => true,
            'cache' => true
        ];
    }

    public function queryUser($host, $authid) {
        $user = new User;

        $this->itemHasAccount($host, $user);

        $user->load(['username = ?', $authid], null, 60);

        return $user;
    }

    public function queryTopic($host, $topic) {
        $resources = new Resource;
        $this->itemHasAccount($host, $resources);

        return $resources->find(['topic = ?', $topic]);
    }

    public function &itemHasAccount($host, &$item) {
        if (preg_match('/^([a-z0-9-]+)\..*/i', $host, $hostMatch)) {
            $item->has('account', ['name = ?', $hostMatch[1]]);
        } else {
            $item->has('account', ['name = ?', 'internal']);
        }

        return $item;
    }

    public function httpAttach(Application $app) {
        $app->attach('auth.', '', [$this, 'httpMap']);
    }

    public function httpMap($map) {
        // $map->host('{subdomain.?}localhost');
        $map->defaults([
            'topic' => ['internal'],
            'subdomain' => 'internal'
        ]);
        // $map->has('has', '/has', [$this, 'httpHas']);
        $map->wildcard('topic');
    }

}