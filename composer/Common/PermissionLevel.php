<?php declare(strict_types = 1);
namespace Common;

use Ducks\Component\SplTypes\SplEnum;

class PermissionLevel extends SplEnum {
    
    const __default = self::None;

    const None = 0;
    const World = 1;
    const Group = 10;
    const Owner = 100;

}
