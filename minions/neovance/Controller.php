<?php declare(strict_types = 1);

namespace NeoVance;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Controller {
    public function __construct(Request $request = null, Response $response = null) {
        $this->request = $request;
        $this->response = $response;
    }

    public function __invoke(Request $request = null, Response $response = null) {
        $this->request = $request ?: $this->request;
        $this->response = $response ?: $this->response;
        $action = $this->request->getAttribute('action');
        $callable = [$this, "{$action}Action"];

        // if (!is_callable($callable)) {
        //     return $this->response->withStatus(404);
        // }
        print_r($this->request->getAttribute('params'));
        
        $ext = call_user_func_array(
            $callable,
            []
        );
        
        return $ext ?: $this->response;
    }

    public function beforeRoute() {}
    public function afterRoute() {}
}
