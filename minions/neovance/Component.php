<?php declare(strict_types = 1);

namespace NeoVance;

use NeoVance\Component\Componentized;

abstract class Component implements Componentized {

    protected $DOM;

    public function __construct($props = []) {
        $this->props = (object)$props;
    }

    protected function beforeRender() {
    }

    protected function afterRender() {
    }

    protected function importNodes($owner, $node) {
        $element = $owner->importNode($node);

        if (!empty($node->childNodes)) {
            foreach($node->childNodes as $child) {
                $next = $this->importNodes($owner, $child);
                $element->appendChild($next);
            }
        }

        return $element;
    }

    protected function nodeIterator($nodes) {
        for($i = $nodes->length; $i >= 0; $i--) {
            $item = $nodes->item($i);
            if ($item && isset($item->tagName)) {
                if (!empty($item->childNodes)) {
                    $this->nodeIterator($item->childNodes);
                }

                if (preg_match('/^([A-Z]{1}.*)/', $item->tagName, $matches)) {
                    $class = "App\\Components\\{$matches[1]}";

                    $props = [ 'children' => $item->childNodes ?: null ];
                    foreach($item->attributes as $name => $attr) {
                        $props[$name] = $attr->nodeValue;
                    }

                    $x = (new $class($props))->getDOM();
                    $sxml = dom_import_simplexml(new \SimpleXMLElement($x));
                    
                    $element = $this->importNodes($item->ownerDocument, $sxml);

                    $item->parentNode->replaceChild(
                        $element,
                        $item
                    );
                }
            }
        };
        return ob_get_contents();
    }

    public function getDOM() {
        $this->beforeRender();
        ob_start();
        $this->render();
        $this->DOM = ob_get_contents();
        ob_end_clean();

        $dom = new \DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;

        try {
            $dom->loadXML($this->DOM);
        } catch(\Throwable $e) {
        }

        $this->DOM = $this->nodeIterator($dom->documentElement->childNodes);
        $this->DOM = $dom->saveHTML();
        $this->afterRender();
        return $this->DOM;
    }

    public abstract function render();

}