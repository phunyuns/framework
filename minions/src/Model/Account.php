<?php declare(strict_types = 1);

namespace Model;

use \DB\Cortex;
use \DB\Jig;

class Account extends Cortex {

    protected $db;
    protected $table = 'accounts';
    protected $fieldConf = [
        'name' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'users' => [
            'has-many' => ['Model\User', 'account']
        ],
        'sessions' => [
            'has-many' => ['Model\Session', 'account']
        ],
        'groups' => [
            'has-many' => ['Model\Group', 'account']
        ],
        'resources' => [
            'has-many' => ['Model\Resource', 'account']
        ]
    ];

    public function __construct() {
        $this->db = new Jig('data/');
        parent::__construct();
    }

}
