<?php declare(strict_types = 1);

namespace Model;

class Session extends Account {

    protected $table = 'sessions';
    protected $fieldConf = [
        'resource' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'user' => [
            'belongs-to-one' => 'Model\User'
        ],
        'account' => [
            'belongs-to-one' => 'Model\Account'
        ],
        'since' => [
            'type' => 'TIMESTAMP',
            'nullable' => false,
            'default' => 'CUR_STAMP'
        ]
    ];

}