<?php declare(strict_types = 1);

namespace Provider;

use \Vinelab\Minion\Provider;
use \NeoVance\Application;
use \Firebase\JWT\JWT;

class Token extends Provider {

    protected $prefix = 'nv.internal.';

    private $private;
    private $public;

    public function boot() {
        $this->private = file_get_contents('/application/private.key');
        $this->public = file_get_contents('/application/pubkey.key');

        $encoder = [$this, 'encodeJWT'];
        $decoder = [$this, 'decodeJWT'];

        $this->register('token.encode', function($args, $kwargs) use ($encoder) {
            return $encoder($args[0], $kwargs->id, $kwargs->head);
        }, [ 'invoke' => 'roundrobin' ]);

        $this->register('token.decode', function($args) use ($decoder) {
            return $decoder($args[0]);
        }, [ 'invoke' => 'roundrobin' ]);
    }

    public function encodeJWT($token, $id = null, $head = null) {
        try {
            return JWT::encode($token, $this->private, 'RS256', $id, $head);
        } catch(\Exception $e) {
            return false;
        }
    }

    public function decodeJWT($token) {
        try {
            return JWT::decode($token, $this->public, ['RS256']);
        } catch(\Exception $e) {
            return false;
        }
    }

}