<?php declare(strict_types = 1);
define('ROOT', __DIR__);

require_once ROOT . '/src/autoload.php';
ini_set('memory_limit', '256M');

use Vinelab\Minion\Minion;

\Cache::instance()->load('memcached=host.docker.internal:11211');

$minion = new Minion;

$minion->register('Provider\Authorization');
$minion->register('Provider\Token');

$minion->run([
    'port' => 9090,
    'host' => 'host.docker.internal',
    'realm' => 'chat',
    'path' => '/ws',
    'debug' => true
]);
