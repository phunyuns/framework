<?php declare(strict_types = 1);

namespace App\Components;

use NeoVance\Component;

class Root extends Component {

    public function render() {
        extract((array)$this->props); ?>
        <!DOCTYPE html>
        <html>
            <Head title="<?php echo $title ?>" />
            <body>
                <?php echo $body ?>
                <script src="https://cdn.jsdelivr.net/npm/autobahn-js-built@0.11.1/autobahn.js" />
                <script src="/application/main.js">
                    { "dependencies": <?php echo json_encode($dependencies) ?> }
                </script>
            </body>
        </html>  
<?php }

}
