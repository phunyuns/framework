<?php declare(strict_types = 1);
define('ROOT', __DIR__);

require_once ROOT . '/src/autoload.php';
ini_set('memory_limit', '256M');

use Vinelab\Minion\Minion;

$minion = new Minion;

$minion->register('Provider\Authentication');
$minion->register('Provider\Token');

$minion->run([
    'port' => 9080,
    'host' => 'host.docker.internal',
    'realm' => 'auth',
    'path' => '/ws',
    'debug' => true
]);
