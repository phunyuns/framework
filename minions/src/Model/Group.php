<?php declare(strict_types = 1);

namespace Model;

class Group extends Account {

    protected $table = 'groups';
    protected $fieldConf = [
        'name' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'value' => [
            'type' => 'INT8',
            'nullable' => false,
            'unique' => true
        ],
        'account' => [
            'belongs-to-one' => 'Model\Account'
        ],
        'users' => [
            'belongs-to-many' => 'Model\User'
        ],
        'resources' => [
            'has-many' => ['Model\Resource', 'group']
        ]
    ];

}