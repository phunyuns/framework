<?php declare(strict_types = 1);

namespace Model;

class Resource extends Account {

    protected $table = 'resources';
    protected $fieldConf = [
        'topic' => [
            'type' => 'VARCHAR256',
            'nullable' => false
        ],
        'match' => [
            'type' => 'VARCHAR128',
            'nullable' => false,
            'default' => 'exact'
        ],
        'account' => [
            'belongs-to-one' => 'Model\Account',
            'nullable' => false
        ],
        'owner' => [
            'belongs-to-one' => 'Model\User',
            'nullable' => false
        ],
        'group' => [
            'belongs-to-one' => 'Model\Group',
            'nullable' => false
        ],
        'grant' => [
            'type' => 'INT4',
            'nullable' => false,
            'default' => 0
        ]
    ];

}