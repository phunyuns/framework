<?php declare(strict_types = 1);

namespace Provider;

use \Vinelab\Minion\Provider;
use \NeoVance\Application;

abstract class HttpProvider extends Provider {

    protected $application;
    protected $version = 1;

    public function __construct($client, $port = 9880) {
        parent::__construct($client);

        $this->application = new Application("/v{$this->version}", [$this, 'httpAttach']);
        $this->application->listen($port, $client->getLoop());
    }

    public abstract function httpAttach(Application $app);

}