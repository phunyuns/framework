<?php declare(strict_types = 1);

namespace NeoVance\Component;

interface Componentized {

    public function getDOM();
    public function render();

}