<?php declare(strict_types = 1);
namespace Common;

use Ducks\Component\SplTypes\SplEnum;

class PermissionType extends SplEnum {
    
    const __default = self::None;

    const None = 0;
    const Execute = 1;
    const Write = 2;
    const Read = 4;

}
