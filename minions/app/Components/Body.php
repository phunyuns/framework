<?php declare(strict_types = 1);

namespace App\Components;

use NeoVance\Component;

class Body extends Component {
    
    protected function beforeRender() {
        call_user_func($this->props->addDependencies, 'Header:Component');
        call_user_func($this->props->addDependencies, 'Sidebar:Component');
    }

    public function render() {
        extract((array)$this->props);
        echo "<div>";
        foreach($dependencies as $dependency) {
            echo "<p>{$dependency}</p>";
        }
        echo "</div>";
    }

}
