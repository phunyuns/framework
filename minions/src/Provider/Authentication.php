<?php declare(strict_types = 1);

namespace Provider;

use \React\Promise\Deferred;
use \NeoVance\Application;
use \Model\User;
use \Model\Account;
use \Model\Group;
use \Dflydev\FigCookies\SetCookie;
use \Dflydev\FigCookies\FigResponseCookies;

class Authentication extends HttpProvider {

    protected $prefix = 'nv.internal.';

    public function boot() {
        $account = new Account;
        $user = new User;
        $group = new Group;

        $account->load(['name = ?', 'internal']);

        if ($account->dry()) {
            $account->reset();
            $account->name = 'internal';
            $account->save();
        }

        $user->load(['username = ? AND account = ?', 'root', $account->_id]);

        if ($user->dry()) {
            $user->reset();
            $user->username = 'root';
            $user->password = 'password';
            $user->email = 'devonbagley@gmail.com';
            $user->account = $account;
            $user->save();
        }

        $group->load(['name = ? AND account = ?', 'root', $account->_id]);

        if ($group->dry()) {
            $group->reset();
            $group->name = 'root';
            $group->value = 1;
            $group->account = $account;
            $group->users[] = $user;
            $group->save();
            $user->groups[] = $group;
            $user->save();
        }

        unset($user, $account, $group);

        $this->register('authenticate', 'authenticate');
    }

    public function authenticate($args) {
        $realm  = array_shift($args);
        $authid = array_shift($args);
        $details = array_shift($args);

        $host = preg_replace('/http:\/\//', '', $details->transport->http_headers_received->origin);

        $user = $this->queryUser($host, $authid);

        if ($user->dry()) {
            return false;
        }

        $userToken = $user->cast();
        unset($userToken['password']);

        print_r(sprintf('Authenticating %s as a user.', $authid));

        $deferred = new Deferred();

        $this->call('token.encode', [$userToken])
            ->then(
                function($token) use ($deferred, $user, $host) {
                    $deferred->resolve([
                        'secret' => $token[0],
                        'role' => 'agent',
                        'extra' => array_merge(
                            ['host' => $host],
                            $user->account->cast()
                        )
                    ]);
                },
                function($err) use ($deferred) {
                    $deferred->resolve(false);
                }
            );

        return $deferred->promise();
    }

    public function queryUser($host, $authid) {
        $user = new User;

        if (preg_match('/^([a-z0-9-]+)\..*/i', $host, $hostMatch)) {
            $user->has('account', ['name = ?', $hostMatch[1]]);
        } else {
            $user->has('account', ['name = ?', 'internal']);
        }

        $user->load(['username = ?', $authid]);

        return $user;
    }

    public function httpAttach(Application $app) {
        $app->attach('auth.', '', [$this, 'httpMapToken']);
    }

    public function httpMapToken($map) {
        // $map->host('{subdomain.?}localhost');
        $map->defaults([
            'subdomain' => 'internal'
        ]);
        $map->post('token', '/token', [$this, 'httpGetToken']);
    }

    public function httpGetToken($request, $response) {
        $params = $request->getParsedBody();

        if (!empty($params)) {
            $user = $this->queryUser($request->getUri()->getHost(), $params->username);
            
            if (!$user->dry()) {
                $bcrypt = \Bcrypt::instance();
                $authorized = $bcrypt->verify(
                    $params->password,
                    $user->password
                );

                if ($authorized) {
                    $userToken = $user->cast();
                    unset($userToken['password']);
                    
                    $deferred = new Deferred();
                    $setCookie = SetCookie::create('chatauth')
                        ->withDomain($request->getUri()->getHost())
                        ->withExpires(
                            (new \DateTime('+2 hours'))->format(\DateTime::COOKIE)
                        )
                        ->withHttpOnly();

                    $this->call('token.encode', [$userToken])
                        ->then(
                            function($token) use ($deferred, $response, $setCookie) {
                                $setCookie = $setCookie->withValue($token[0]);
                                $response = FigResponseCookies::set($response, $setCookie);
                                $response->getBody()
                                    ->write(json_encode([
                                        'token' => $token[0]
                                    ]));
                                $deferred->resolve($response);
                            },
                            function($err) use ($deferred) {
                                $deferred->resolve($response);
                            }
                        );

                    return $deferred->promise();
                }
            }
        }

        $response = $response->withStatus(403);
        $response->getBody()->write(json_encode([
            'error' => [
                'message' => 'Not Authorized',
                'code' => 403
            ],
        ]));

        return $response;
    }

}