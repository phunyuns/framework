<?php declare(strict_types = 1);

namespace Model;

class User extends Account {

    protected $table = 'users';
    protected $fieldConf = [
        'username' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'password' => [
            'type' => 'VARCHAR256',
            'nullable' => false
        ],
        'email' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'show' => [
            'type' => 'INT4',
            'nullable' => false,
            'default' => 0
        ],
        'status' => [
            'type' => 'TEXT'
        ],
        'account' => [
            'belongs-to-one' => 'Model\Account'
        ],
        'sessions' => [
            'has-many' => ['Model\Session', 'user']
        ],
        'groups' => [
            'belongs-to-many' => 'Model\Group'
        ],
        'resources' => [
            'has-many' => ['Model\Resource', 'owner']
        ]
    ];

    public function set_email($value) {
        if (\Audit::instance()->email($value) == false) {
            // no valid email address
            // throw exception or set an error var and display a flash message
            $value = null;
        }
        return $value;
    }

    // hash a password before saving
    public function set_password($value) {
        return \Bcrypt::instance()->hash($value);
    }

}