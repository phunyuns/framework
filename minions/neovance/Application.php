<?php declare(strict_types = 1);

namespace NeoVance;

use \Auryn\Injector;
use \Nyholm\Psr7\Response;
use \Aura\Router\Route;
use \React\EventLoop\LoopInterface;
use \React\EventLoop\Factory as LoopFactory;
use \React\Socket\Server as Socket;
use \React\Http\Server;
use \Psr\Http\Message\ServerRequestInterface;

/**
 * Provide entry point for application execution
 */
class Application {

    protected $injector;
    protected $dependencies = [
        'request' => 'delegate',
        'map' => 'delegate',
        'matcher' => 'delegate',
        'route' => 'delegate',
        'response' => 'delegate'
    ];
    protected $shared = [
        'Nyholm\Psr7\Factory\MessageFactory',
        'Nyholm\Psr7\Factory\ServerRequestFactory',
        'Aura\Router\RouterContainer',
        'Aura\Router\Map',
        'Aura\Router\Matcher'
    ];
    
    public function __construct($base = '/', Callable $routerCallback = null)
    {
        $this->injector = new Injector;

        $this->injector->define('Aura\Router\RouterContainer', [$base]);
        
        foreach ($this->shared as $shared) {
            $this->injector->share($shared);
        }

        foreach ($this->dependencies as $prepare => $type) {
            $prepName = ucfirst($prepare);
            $function = "prepare{$prepName}";

            if (method_exists($this, $function)) {
                $userFunc = [$this->injector, $type];
                call_user_func_array($userFunc, $this->$function());
            }
        }

        $this->initializeRouting($routerCallback);
    }

    protected function initializeRouting(Callable $routerCallback = null) {
        $injector =& $this->injector;

        $this->injector->alias('Psr\Http\Message\ResponseInterface', 'Nyholm\Psr7\Response');
        $this->injector->delegate('Nyholm\Psr7\ServerRequest', function() use ($injector) {
            $request = $injector->make('Psr\Http\Message\ServerRequestInterface');
            $route = $injector->make('Aura\Router\Route');
            foreach ($route->attributes as $key => $val) {
                $request = $request->withAttribute($key, $val);
            }
            return $request;
        });

        if ($routerCallback && is_callable($routerCallback)) {
            call_user_func($routerCallback, $this);
        }

        // $this->route('default', '{/controller,action}', 'App\Controllers\IndexController')
        //     ->allows('GET')
        //     ->defaults([
        //         'controller' => 'index',
        //         'action' => 'index',
        //         'params' => [],
        //     ])
        //     ->wildcard('params');

        // $this->route('assets', '/application/{filename}', function($request, $response) {
        //     $response = $response->withHeader('Content-Type', 'application/javascript');
        //     $response->getBody()->write(
        //         file_get_contents(__DIR__ . "/../public/application/{$request->getAttribute('filename')}")
        //     );

        //     return $response;
        // });
            
        // function($request, $response) {
        //     try {
        //         (new \App\Controllers\IndexController($request, $response))->indexAction();
        //     } catch (\Throwable $e) {
        //         $response->getBody()->write(print_r($e, true));
        //     }
        //     return $response;
        // });
    }

    protected function prepareResponse() {
        return [
            'Nyholm\Psr7\Response',
            [
                'Nyholm\Psr7\Factory\MessageFactory',
                'createResponse'
            ]
        ];
    }

    protected function prepareRoute() {
        return [
            'Aura\Router\Route',
            [
                'Aura\Router\Matcher',
                'match'
            ]
        ];
    }

    protected function prepareMatcher() {
        return [
            'Aura\Router\Matcher',
            [
                'Aura\Router\RouterContainer',
                'getMatcher'
            ]
        ];
    }

    protected function prepareMap() {
        return [
            'Aura\Router\Map',
            [
                'Aura\Router\RouterContainer',
                'getMap'
            ]
        ];
    }

    protected function prepareRequest() {
        return [
            'Psr\Http\Message\ServerRequestInterface',
            [
                'Nyholm\Psr7\Factory\ServerRequestFactory',
                'createServerRequestFromGlobals'
            ]
        ];
    }

    public function __call($verb, $args) {
        $map = $this->injector->make('Aura\Router\Map');

        if (method_exists($map, $verb) || is_callable([$map, $verb])) {
            return call_user_func_array([$map, $verb], $args);
        }

        if (method_exists($this->injector, $verb)) {
            return call_user_func_array([$this->injector, $verb], $args);
        }
    }

    /**
     * Execute application instance
     */
    public function main(ServerRequestInterface $request, Response $response, Route $route)
    {
        try {
            if (is_callable($route->handler)) {
                return call_user_func($route->handler, $request, $response);
            }
        } catch (\Throwable $e) {
            $response->getBody()->write(print_r($e->getMessage(), true));
            return $response;
        }

        return $response;

        $controller = preg_replace(
            '/\[controller\]/i',
            ucfirst($route->attributes['controller']),
            $route->handler
        );
        
        $action = "{$route->attributes['action']}Action";
        $handler = $this->injector->make($controller, [
            ':request' => $request,
            ':response' => $response
        ]);

        if (is_callable([$handler, 'beforeRoute'])) {
            $this->injector->execute([$handler, 'beforeRoute']);
        }
        
        $response = $this->injector->execute(
            [$handler, $action],
            $route->attributes['params']
        );

        if (is_callable([$handler, 'afterRoute'])) {
            $this->injector->execute([$handler, 'afterRoute']);
        }

        return $response;
    }

    public function listen($port = 80, LoopInterface $loop = null) {
        $eventLoop = $loop ?: LoopFactory::create();
        $socket = new Socket("0.0.0.0:$port", $eventLoop);

        $main = [&$this, 'main'];
        $injector =& $this->injector;

        print_r(sprintf("Getting ready for connections on %s.\n", (string)$port));

        $server = new Server([
            function ($request, $next) {
                $type = strtolower($request->getHeaderLine('Content-Type'));
                list ($type) = explode(';', $type);

                if ($type === 'application/json') {
                    $body = $request->getBody();
                    $json = json_decode($body->read($body->getSize()));
                    return $next($request->withParsedBody($json));
                }

                return $next($request);
            },
            function ($request) use ($main, $injector) {
                $matcher = $injector->make('Aura\Router\Matcher');
                $route = $matcher->match($request);

                print_r("New incomming request.\n");
                print_r(sprintf("Request from: %s\n", (string)$request->getHeaderLine('Host')));

                if ($route) {
                    foreach ($route->attributes as $key => $val) {
                        $request = $request->withAttribute($key, $val);
                    }
                } else {
                    $response = $injector->make('Nyholm\Psr7\Response');
                    $response = $response->withStatus(404);
                    return $response;
                }

                $response = $injector->execute($main, [
                    ':request' => $request,
                    ':route' => $route
                ]);

                if (is_a($response, 'React\Promise\Promise')) {
                    return $response->then(function($real) {
                        return $real->withHeader('content-type', 'application/json');
                    });
                }

                return $response->withHeader('content-type', 'application/json');
            }
        ]);

        $server->on('error', function($e) {
            if (is_array($e)) {
                foreach($e as $error) {
                    print_r($error->getMessage()."\n");
                }
            }

            print_r($e->getMessage()."\n");
        });

        $server->listen($socket);

        return $eventLoop;
    }

    /**
     * Proper shite
     */
    public function run()
    {
        $response = $this->injector->execute([$this, 'main']) ?:
            $this->injector->make('Nyholm\Psr7\Response');
        foreach ($response->getHeaders() as $name => $values) {
            foreach ($values as $value) {
                header(sprintf('%s: %s', $name, $value), false);
            }
        }
        http_response_code($response->getStatusCode());
        echo $response->getBody();
    }

}