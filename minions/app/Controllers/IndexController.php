<?php declare(strict_types = 1);

namespace App\Controllers;

use \NeoVance\Controller;

class IndexController extends Controller {

    public function indexAction() {
        $dependencies = [];
        $addDependencies = function(...$depends) use (&$addDependencies, &$dependencies) {
            foreach($depends as $dependency) {
                if (is_array($dependency)) {
                    call_user_func_array($addDependencies, $dependency);
                } else {
                    array_push($dependencies, $dependency);
                }
            }
        };

        $props = [
            'title' => 'Home',
            'request' => $this->request,
            'dependencies' => &$dependencies,
            'addDependencies' => &$addDependencies,
        ];

        $props['body'] = (new \App\Components\Body($props))->getDOM();

        $this->response->getBody()
            ->write((new \App\Components\Root($props))->getDOM());
    }

    public function applicationAction($filename) {
        $response = $this->response->withHeader('Content-Type', 'application/javascript');

        $response->getBody()->write(
            file_get_contents(__DIR__ . "/../../public/application/{$filename}")
        );

        return $response;
    }

}